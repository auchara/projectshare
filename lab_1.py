# -*- coding: utf-8 -*-
# print "hello world"
# num1 = 5
# num2 = 3
# num3 = 7
# answer = num1 + num2 + num3
# print "answer = ", answer
#
# def volumeOfsquare(input):
#     answer = input + input + input
#     print answer
# volumeOfsquare(num1)
# volumeOfsquare(num2)
# volumeOfsquare(num3)
#
# variable_1 = "dog cat bird"
# print "you can print string directly"
# print variable_1
# print "\n"
# print "your string is too long? \n press  back slash n"
# print "your string is too long? "\
#         "press enter or use \ "
#
# print "welcome to work shop class \n"
# print "\n"
# print 'you should choose double quote or single quote'
#
# print "welcome to work shop class \n"
# print 'you should choose double quote or single quote \n'
#
# variable_1 = "you can connect string"
# variable_2 = "by use +"
# print variable_1 + variable_2
# print ""
# variable_1 = "ten ="
# variable_2 = 10
# print variable_1, variable_2
# print ""
# variable_1 = "fish"
# print "cat eat %s" %variable_1
#
# # .format()
# print "cat eat {}.".format(variable_1)
# print "cat eat {1} and {1}".format(variable_1, variable_1)
#
#
# vari_1 = "this is string"
# vari_1 = 55
# print vari_1
# print type(vari_1)
#
# num1 = 5
# num2 = 3
# print "num1 + num2 = ",num1+num2
# print "num1 - num2 = ", num1-num2
# print "num1 * num2 = ", num1*num2
# print "num1 / num2 =", num1/num2
# print "num1 // num2 = ", num1//num2
# print "num1 % num2 = ", num1 % num2
# print "num1 ** num2 = ", num1**num2

# num1 = 5.0
# num2 = 2
# r = num1/num2
# rr = num1//num2
# print "num1 / num2 = %f" % r
# print "num1 / num2 = %f" % (num1/num2)
# print "num1 / num2 = %f" % rr
# print "num1 / num2 = %f" % (num1//num2)

# num1 = 5
# num2 = 3
# num3 = 7
# print "First line answer = ", num1*num2-num3
# print "Second line answer = ", num1*(num2-num3)

# num1 = 18
# num2 = 6
# num3 = 4
# num4 = 1
# print "First line answer = ", num1/num2*(num3-num4)

# num1_1, num1_2, num1_3 = 10, 15, 10
# num2_1, num2_2, num2_3 = 2, 10, 25
# boxnum1 = num1_1*num1_2*num1_3
# boxnum2 = num2_1 * num2_2 * num2_3
# print "First line answer = ", boxnum1
# print "Second line answer = ", boxnum2
# answer = boxnum1 / boxnum2
# print "answer = ", answer

# num1 = 5
# num2 = 3
# num3 = 7
# answer = num1*num1*num1
# print "answer = ", answer
# answer = num2*num2*num2
# print "answer = ", answer
# answer = num3*num3*num3
# print "answer = ", answer

# num1 = 5
# num2 = 3
# num3 = 7
#
#
# def volume_of_square(input_num):
#       answer = input_num*input_num*input_num
#       print "answer = ", answer
#
#
# volume_of_square(input_num=num1)
# volume_of_square(input_num=num2)
# volume_of_square(input_num=num3)


# def myFunction():
#     print "Hello function"
#
#
# print "not function"
# myFunction()

# num1 = 2
# num2 = 3
#
#
# def plus(integer_1, integer_2):
#       ans_1 = integer_1 + integer_1
#       ans_2 = integer_2 - integer_2
#       return ans_1, ans_2
#
#
# ans_1, ans_2 = plus(num1, num2)
# print "answer = ", ans_1, ans_2


# def volume_of_square(r,h):
#       answer = (3.141*(r**2)*h)/3
#       print "answer = %.3f " %answer
#
# volume_of_square(3,5)


# def volume_of_square(r, h):
# #     answer = (1.0/3)*3.141 * (r ** 2) * h
# #     print "answer = %.3f " % answer
# #
# #
# # volume_of_square(3, 5)

# pi = 3.141
# h = input("high: ")
# r = input("red: ")
#
# def volume(high, red) :
#     print (high*(red**2)*pi)/3
#
#
# volume(h,r)

# a = 5
# b = 2
#
# x = (a == b)
# x1 = (a != b)
# x2 = (a > b)
# x3 = (a < b)
# x4 = (a >= b)
# x5 = (a <= b)
#
# print x
# print x1
# print x2
# print x3
# print x4
# print x5

# a=5
# b=2
#
# print (a == b)
# print (a != b)
# print (a > b)
# print (a < b)
# print (a >= b)
# print (a <= b)

# num = input("num = ")
# if num % 2 == 0:
#     print "even"
# else:
#     print "odd"

#
# score = input("score = ")
# test = input("test = ")
# total = score + test
# if total >= 80:
#     print "A"
# elif total >= 70:
#     print "B"
# elif total >= 60:
#     print "C"
# elif total >= 50:
#     print "D"
# else:
#     print "F"

# for x in "word":
#      print x
#
# for x in range(12):
#     print x

# for x in range(1,5):
#     print x

# x1 = input("num = ")
# for x in range(1,13):
#     a = x1 * x
#     print "%d * %d = %d" %(x1, x, a)
#
# num = 1
# while num <= 5:
#     print num
#     num = num + 1
#
# num = 1
# while num < 5:
#     print num
#     num = num + 1
#

# def score_of_test():
#     score = input("score = ")
#     print "คะแนนที่ได้: " , score
#     num = score
#     if num >= 90:
#         print "เก่งมาก สอบผ่าน"
#     elif num >= 60:
#         print "สอบผ่าน"
#     else:
#         print "สอบตก"
# while True :
#     score_of_test()

# homework
# def score_of_test():
#     score = input("score = ")
#     print "คะแนนที่ได้: " , score
#     if score > 100 or score < 0:
#         print "ผิดพลาด"
#     elif score >= 90:
#         print "เก่งมาก สอบผ่าน"
#     elif score >= 60:
#         print "สอบผ่าน"
#     else:
#         print "สอบตก"
# while True:
#     score_of_test()


# all_index = range(1, 13)
# print all_index
# start = True
#
#
# def my_func():
#     num_set = input("Number: ")
#     for i in all_index:
#         result = i * num_set
#         print "{1} x {0} = {2}".format(num_set, i, result)
#
#     if num_set == 12:
#         start_status = False
#         return start_status
#     else:
#         start_status = True
#         return start_status
#
#
# while start:
#     start = my_func()
#     print "Loop status: ", start

# empty list
# my_list = []

# list of integers
# my_list = [1, 2, 3]

# list with mixed datatypes
# my_list = [1, "Hello", 3.4]

# list_fruit = ["apple", "banana", "cherry"]
# list_fruit.append("mamgo")
# list_fruit.remove(list_fruit[0])
# print list_fruit
# print range(len(list_fruit))
# print list_fruit[1]
# เช็กจำนวนตัวอักษรในlist
# for index in range(len(list_fruit)):
#     print index, list_fruit[index], ">>>>", len(list_fruit[index])

# แก้ b>m
# list_fruit[1] = "mango"
# print list_fruit

# print list_fruit

# รับค่าเ index เข้ามา และแก้ไข index นั้น เป็น ข้อความอะไรก็ได้
# list_fruit = ["apple", "banana", "cherry"]
# list_fruit.append("mango")
# print list_fruit
# print range(len(list_fruit))
#
# def my_func():
#     x = input("index is >>> ")
#     a = raw_input("text is >>>> ")
#     list_fruit[x] = a
#     print list_fruit
#     if len(list_fruit[x]-1) <= x:
#
# my_func()
#
# while True:
#     my_func()

# list_fruit = ["apple", "banana", "cherry", "mango"]
# print list_fruit[2]
# list_fruit.append("orange")
# list_fruit.append("coconut")
# print list_fruit
# list_fruit[0] = "football"
# print list_fruit
# list_fruit.remove(list_fruit[1])
# print list_fruit
# print len(list_fruit)

# list_fruit = ["apple", "banana", "cherry", "mango"]
# print "1", list_fruit[2]
# list_fruit.append("orange")
# list_fruit.append("coconut")
# list_fruit.extend(["orange", "coconut"])
# print "2", list_fruit
# list_fruit[0] = "football"
# print "3", list_fruit
# list_fruit.remove(list_fruit[1])
# print "4", list_fruit
# print "5", len(list_fruit)

# การประกาศ
# my_dict = {"date": 3, "month": "December", "year": 2018}
# print my_dict

# ดึงออกมาใช้
# my_dict = {"date": 3, "month": "December", "year": 2018}
# print my_dict["month"]

# add
# my_dict = {"date": 3, "month":  "December", "year": 2018}
# print my_dict
# my_dict["day"] = "monday"
# print my_dict
#
# my_dict = {"date":  3, "month": "December", "year": 2018}
# print my_dict.keys()

# car = {"brand": "Ford", "model": "Mustang", "year": 1964}
# car["color"] = "pink"
# print car
# print car["model"]
# print len(car)

# list_fruit = ["apple", "banana", "cherry"]
#
# for fruit in list_fruit:
#     print fruit
#
# for fruit in range(len(list_fruit)):
#     print list_fruit[fruit]

#
# data = [
#     {"output1": ["MS_AOTBTTEXCH01_DB7",
#                  "HP_D2_SATA_TRUE-CJ_01",
#                  "SPARC_RedCross_DB",
#                  "SPARC_TOPGUN_OS"]
#      },
#     {"output2": ["MS_IDC1_MS_ACI3_PRD01_SAS01",
#                  "BTT_EX04_Prod_HMDB10GB01_AC",
#                  "HP_D1_SAS_SERISYS_2"]
#      }
# ]
# target = data[1]["output2"][1]
# print target
# print data[0]["output1"]

# a = data[0]["output1"]
# for b in a:
#     print b
# c = data[1]["output2"]
# for d in c:
#     print d

# my_dict  =  {
#   "date" :  3,
#   "month" :  "December",
#   "year": 2018
# }
# for x in my_dict :
#     print  x
#
# for x in my_dict.values() :
#     print  x

# list_name = ["Alice", "Bob", "Oscar", "Alice", "Alice", "Bob", "Oscar", "Bob"]
# list_money =[100, 200, 150, 50, 80, 120, 30, 180]
#
# my_dict = {}
# def main():
#     for index in range(len(list_name)):
#         key_name = list_name[index]
#         if key_name in my_dict:
#             new_value = my_dict[key_name] + list_money[index]
#             my_dict[key_name] = new_value
#         else:
#             my_dict[key_name] = list_money[index]
#     print my_dict
# main()

import datetime
# now = datetime.datetime.now()
# new_date = datetime.datetime (year=2018,month=1,day=1,hour=10,minute=0,second=0)
# print now
# print new_date #2018-01-01 10:00:00

# date_str = "10/11/2012 13:14:15"
# date = datetime.datetime.strptime (date_str,"%d/%m/%Y %H:%M:%S")
# print date #2012-11-10 13:14:15
# print type(date) #<type 'datetime.datetime'>

# print date #2012-11-10 13:14:15
# print type(date) #<type 'datetime.datetime'>
# date = date.strftime("%d/%m/%Y %I:%M:%S %p")
# print date #2012/11/10 01:14:15 PM
# print type(date) #<type 'str'>

# now = datetime.datetime.now()
# print now
# print datetime.timedelta(minutes=2)#0:02:00
# print now-datetime.timedelta(minutes=2)
# print now > datetime.datetime(2012,11,10) #True
# print now < now-datetime.timedelta(minutes=2) #False

# print "My Birth day is:"
# aa_1 = input("Day: ")
# bb_1 = input("Month: ")
# cc_1 = input("Year: ")
# day = datetime.datetime(year = cc_1, month= bb_1, day = aa_1)
# now = datetime.datetime.now()
# day_1 = now - day
# print day_1

# data = "zvzry nzw day is anothzr chancz to changz your lifz."
# x = data.replace("z", "e")
# print x.capitalize()


# data = """Interface            	IP-Address       OK? Method  Status           Protocol
#           GigabitEthernet1       192.168.1.3       YES  manual  down             up
#           GigabitEthernet2       172.17.111.3     YES  manual    up               up
#           GigabitEthernet3       192.168.101.3   YES  manual    up                up"""
# for line in data.splitlines():
#     if line.split()[4] == "down":
#         print line.split()[0], ":", line.split()[4]

# data="""Filesystem  Size  Used Avail Use% Mounted on
#        	tmpfs   3.9G     0  3.9G   0% /dev/shm
#        	tmpfs   3.9G   26M  3.8G   1% /run
#        	tmpfs   3.9G     0  3.9G   0% /sys/fs/cgroup"""
# print "SplitLine:",data.splitlines()
# for line in data.splitlines():
#        print line.split()[0],":",line.split()[1]


# data = """Python is a language
# used to create a web,
# desktop application
# and more"""
# print data.splitlines()
# print data.split()

# def my_func():
#     i = input("Number:")
#     num = i % 1
#     if num >= 0.5:
#         new_n = i + 1 - num
#         print ">>> %d" % new_n
#     else:
#         print ">>> %d " % i
#
# while True:
#     my_func()

# my_list = [-1, 8, 4, 5, -11, -3, 7, 1, 0, 2, -9]
# integer_list =[]
# def my_func():
#     for index in range(len(my_list)):
#         list_1 = my_list[index]
#         if list_1 >= 0:
#             integer_list.append(list_1)
#     integer_list.sort()
#     print ">>>>", integer_list
# my_func()

# data = "Python"
# print data[5]
# print data[4]
#
# data = "python"
# print data[0:2]
# print data[1:4]

# data = "python"
# print data.startswith("py")
# print data.endswith("th")

# data = "this is a python"
# print data.capitalize()
# print data.title()
# print data.upper()
# print data.lower()

# colon = ">>>"
# character = ["a","b","c"]
# print colon.join(character)


# data = "One:Two:Three:Four:Five"
# print data.split(":")
# print data.split(":",2)

# while True:
#     try:
#         a = float(input("Enter first number: "))
#         b = float(input("Enter second number: "))
#         print "%d / %d =  %f" % (a, b, a / b)
#     except:
#         print "Invalid Input"

# print 10/0
# print 5* money

# try:
#     x = input("input: ")
#     cal = 10/x
# except ZeroDivisionError:
#     print("เกิดข้อผิดพลาดในการแปล")

# try:
#     name = raw_input("Enter your name: ")
#     if name == 'mateo':
#         raise Exception("Whoa! Mateo you are not allowed here")
#     print "Hi ", name
# except Exception as err:
#     print "Exception: ", err
# else:
#     print "Welcome"
# finally:
#     print "===END==="

while True:
    try:
        m = input("มวล: ")
        print type(m)
        p = input("ปริมาตร: ")
        print type(p)
        d = m / p
        print d
        print type(d)
    except ZeroDivisionError as e:
        print ("ไม่สามารถหารด้วย 0 ได้")
    except TypeError as e:
        print ("ใช้ตัวแปรผิดชนิด")
    except NameError as e:
        print ("ไม่สามารถใส่ตัวอักษรได้")
    else:
        print "ความหนาแน่น มีค่าเท่ากับ", d , "\nการหาความหนาแน่นเสร็จสิ้น"
    finally:
        print ("ขอบคุณค่ะ")

