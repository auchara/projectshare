# import yaml
# with open("lab3.yaml") as f:
# 	f = yaml.safe_load(f)
	# print f["bob"]
	# print f["bob"]["age"]
	# print f["alice"]
	# print f["alice"]["color"]
	# for name in f.keys():
	# 	if name == "bob":
	# 		print f[name]


import time
import threading

# def cal_max(numbers, delay):
# 	time.sleep(delay)
# 	print "Max:", max(numbers)
#
# def cal_min(numbers, delay):
# 	time.sleep(delay)
# 	print "Min:", min(numbers)
# start_time = time.time()
# arr = [10, 7, 29, 62, 19]
# delay1 = 3
# delay2 = 5
# cal_max(arr, delay1)
# cal_min(arr, delay2)
# print "Elapsed time:", time.time() - start_time

# thread1 = threading.Thread(target=cal_max, args=(arr, delay1,))
# thread2 = threading.Thread(target=cal_min, args=(arr, delay2,))
#
# thread1.start()
# thread2.start()
#
# thread1.join()
# thread2.join()
# print "Elapsed time:", time.time() - start_time

def cal_S(numbers, delay):
	time.sleep(delay)
	print "Square:", (numbers)

def cal_Q(numbers, delay):
	time.sleep(delay)
	print "Cube:", (numbers)

num = input("Number: ")
start_time = time.time()
delay1 = 2
delay2 = 3
arr1 = num*num
arr2 = num*num*num
cal_S(arr1, delay1)
cal_Q(arr2, delay2)
print "Elapsed time:", time.time() - start_time

start_time = time.time()
thread1 = threading.Thread(target=cal_S, args=(arr1, delay1,))
thread2 = threading.Thread(target=cal_Q, args=(arr2, delay2,))

thread1.start()
thread2.start()

thread1.join()
thread2.join()
print "Elapsed time:", time.time() - start_time